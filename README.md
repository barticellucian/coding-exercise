----
Hello, 
I am Lucian.
This repo contains my solution for the coding exercise.

The only ambiguous part of the problem for me, was the first one. I am not sure how they should load entries/ which entries.
So I implemented all the others, but not this part.

I appreciate there are a large number of dependencies and I know this exercise can be done without many of them, 
but they are there so I can show usage of:

- react

- webpack

- es2015

- es2017 <- async/await

- nodejs/express

- socket.io


---
1. Get started:
npm install

2. Run it:
npm start

3. Test it:
npm test

