import React from "react";
import style from "./style.scss";
import isValid from "../../utils/isValid";

const socket = io();

export default class AddForm extends React.Component{
	constructor(props){
        super(props);
        this.state = {
        	firstName: "", 
        	surname: "",
        	contact_number: "",
        	email: ""
        };
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleSurnameChange = this.handleSurnameChange.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
 	}

 	handleFirstNameChange(e)
 	{
 		this.setState({firstName: e.target.value});
 	}
 	handleSurnameChange(e)
 	{
 		this.setState({surname: e.target.value});
 	}
 	handleNumberChange(e)
 	{
 		this.setState({contact_number: e.target.value});
 	}
 	handleEmailChange(e)
 	{
 		this.setState({email: e.target.value});
 	}
 	handleFormSubmit(e)
 	{	
 		e.preventDefault();
 		if(isValid(this.state)){
 			socket.emit('add-entry', JSON.stringify(this.state));	
 		}else{
 			alert("Your entry is not valid. Please check the fields again");
 			this.forceUpdate();
 		}
 	}
 	

	render(){
		return <div id="addForm">
	          <form onSubmit={this.handleFormSubmit}>
	          	<input type="text" name="firstName" placeholder="First Name" value={this.state.firstName} onChange={this.handleFirstNameChange}/>
	          	<input type="text" name="surname" placeholder="Surname" value={this.state.surname} onChange={this.handleSurnameChange}/><br/>
	          	<input type="text" name="number" placeholder="Contact Number" value={this.state.contact_number} onChange={this.handleNumberChange}/>
	          	<input type="email" name="email" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange}/>
	          	<button type="submit">Add</button>
	          </form>
	        </div>;
	}
};