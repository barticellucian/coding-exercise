import React from "react";
// import style from "./style.scss";
const socket = io();


export default class ListingTable extends React.Component{
	constructor(props){
      super(props);
      this.initialEntry = this.props.entry;
      this.state = this.props.entry;
      this.deleteEntry = this.deleteEntry.bind(this);
      this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
      this.handleFirstNameEdit = this.handleFirstNameEdit.bind(this);
      this.handleSurnameChange = this.handleSurnameChange.bind(this);
      this.handleSurnameEdit = this.handleSurnameEdit.bind(this);
      this.handleNumberChange = this.handleNumberChange.bind(this);
      this.handleNumberEdit = this.handleNumberEdit.bind(this);
      this.handleEmailChange = this.handleEmailChange.bind(this);
      this.handleEmailEdit = this.handleEmailEdit.bind(this);
	}


  handleFirstNameChange(e)
  {
    this.setState({firstName: e.target.value});
  }
  handleFirstNameEdit(e)
  {
    if(this.nameIsValid(e.target.value)){
      socket.emit("edit-entry", JSON.stringify([this.initialEntry, this.state]));
    }else{
      this.setState({firstName: this.initialEntry.firstName});
      alert("first name is not valid");
    }
  }
  handleSurnameChange(e)
  {
    this.setState({surname: e.target.value});
  }
  handleSurnameEdit(e)
  {
    if(this.nameIsValid(e.target.value)){
      socket.emit("edit-entry", JSON.stringify([this.initialEntry, this.state]));
    }else{
      this.setState({surname: this.initialEntry.surname});
      alert("surnname is not valid");
    }
  }
  handleNumberChange(e)
  {
    this.setState({contact_number: e.target.value});
  }
  handleNumberEdit(e)
  {
    if(this.numberIsValid(e.target.value)){
      socket.emit("edit-entry", JSON.stringify([this.initialEntry, this.state]));
    }else{
      this.setState({contact_number: this.initialEntry.contact_number});
      alert("number is not valid");
    }
  }
  handleEmailChange(e)
  {
    this.setState({email: e.target.value});
  }
  handleEmailEdit(e)
  {
    if(this.emailIsValid(e.target.value)){
      socket.emit("edit-entry", JSON.stringify([this.initialEntry, this.state]));
    }else{
      this.setState({email: this.initialEntry.email});
      alert("email is not valid");
    }
  }

  nameIsValid(name)
  {
    const nameRegex = new RegExp(/^[a-zA-Z0-9]*$/);
    if(!nameRegex.test(name) || name.length > 50 || name.length === 0)
      return false;
    return true;
  }
  numberIsValid(number)
  {
    return true;
    // const numberRegex = new RegExp(/^\+?\d+$/);
    // if(!numberRegex.test(this.state.contact_number) || this.state.contact_number<7 || this.state.contact_number>15)
    //     return false;
    //  return true; 
  }
  emailIsValid(email)
  {
    const emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if(!emailRegex.test(email))
      return false
    return true;
  }

  deleteEntry()
  {
    socket.emit("delete-entry", JSON.stringify(this.initialEntry));
  }

	render(){
		return <tr className="tableEntry">
              <td><input type="text" value={this.state.firstName} onChange={this.handleFirstNameChange} onBlur={this.handleFirstNameEdit} /></td>
              <td><input type="text" value={this.state.surname} onChange={this.handleSurnameChange} onBlur={this.handleSurnameEdit} /></td>
              <td><input type="text" value={this.state.contact_number} onChange={this.handleNumberChange} onBlur={this.handleNumberEdit} /></td>
              <td><input type="text" value={this.state.email} onChange={this.handleEmailChange} onBlur={this.handleEmailEdit} /></td>
              <td><button onClick={this.deleteEntry}>X</button></td>
            </tr>;
	}
};