import React from "react";
import style from "./style.scss";
import Entry from "../Entry/Entry";


export default class ListingTable extends React.Component{
	constructor(props){
        super(props);
        this.socket = null;
        this.entries = [];
        this.sorting = null;
        this.handleSortByFirstName = this.handleSortByFirstName.bind(this);
        this.handleSortBySurname = this.handleSortBySurname.bind(this);
        this.handleSortByEmail = this.handleSortByEmail.bind(this);
        this.handleTriggerUpload = this.handleTriggerUpload.bind(this);
        this.handleFileChosen = this.handleFileChosen.bind(this);
  	}

  	uuid() 
  	{
		/*jshint bitwise:false */
		let i, random;
		let uuid = '';

		for (i = 0; i < 32; i++) {
			random = Math.random() * 16 | 0;
			if (i === 8 || i === 12 || i === 16 || i === 20) {
				uuid += '-';
			}
			uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
				.toString(16);
		}

		return uuid;
	}



  	componentDidMount()
  	{
  		this.socket = io();
      this.socket.emit('get-entries', "");
  		this.socket.on("gotEntries", (data)=>{
  			let entries = JSON.parse(data);
  			this.entries = entries.map((entry, index)=>{
  				return <Entry key={this.uuid()} entry={entry}></Entry>;
  			})
  			if(this.sorting){
  				switch(this.sorting){
  					case "firstName":
  						this.sortByFirstName();
  						break;
  					case "surname":
  						this.sortBySurname();
  						break;	
					case "email":
  						this.sortByEmail();
  						break;	
  				}
  			}
  			this.forceUpdate();
  		})
  		this.socket.on("serverError", (msg)=>{
  			alert(msg);
  		})
  	}

  	sortByFirstName()
  	{
  		this.entries.sort((a,b)=>{
  			if (a.props.entry.firstName < b.props.entry.firstName)
				return -1;
			if (a.props.entry.firstName > b.props.entry.firstName)
				return 1;
			return 0;
  		})
  	}
  	sortBySurname()
  	{
  		this.entries.sort((a,b)=>{
  			if (a.props.entry.surname < b.props.entry.surname)
				return -1;
			if (a.props.entry.surname > b.props.entry.surname)
				return 1;
			return 0;
  		})
  	}
  	sortByEmail()
  	{
  		this.entries.sort((a,b)=>{
  			if (a.props.entry.email < b.props.entry.email)
				return -1;
			if (a.props.entry.email > b.props.entry.email)
				return 1;
			return 0;
  		})
  	}

  	handleSortByFirstName()
  	{
  		this.sorting = "firstName";
  		this.sortByFirstName();
  		this.forceUpdate();
  	}
  	handleSortBySurname()
  	{
  		this.sorting = "surname";	
  		this.sortBySurname();
  		this.forceUpdate();
  	}
  	handleSortByEmail()
  	{
  		this.sorting = "email";
  		this.sortByEmail();
  		this.forceUpdate();
  	}
    handleTriggerUpload()
    {
      // this.uploadInput.click();
    }
    handleFileChosen(e)
    {
      // console.log(e.target.files[0].name);
    }

	render(){
		return <div id="listingTable">
				<h2>Employee Data</h2>
				<button id="loadData" type="file" onClick={this.handleTriggerUpload}>Load</button>
				<table>
					<thead>
						<tr>
							<th className="sortable" onClick={this.handleSortByFirstName}>First Name</th>
							<th className="sortable" onClick={this.handleSortBySurname}>Surname</th>
							<th>Contact Number</th>
							<th className="sortable" onClick={this.handleSortByEmail}>Email</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						{this.entries}
					</tbody>
				</table>
            </div>;
	}
};

// <input type="file" id="uploadField" ref={el=> {this.uploadInput = el; }} onChange={this.handleFileChosen} />