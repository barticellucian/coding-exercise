import React from "react";
import AddForm from "../../components/AddForm/AddForm";
import ListingTable from "../../components/ListingTable/ListingTable";

export default class View extends React.Component{
	constructor(props){
        super(props);
  }

	render(){
		return <div id="view">
              <AddForm></AddForm>
              <ListingTable></ListingTable>
            </div>;
	}
};