export default function isValid(entry)
{
	const nameRegex = new RegExp(/^[a-zA-Z0-9]*$/);
	const emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	const numberRegex = new RegExp(/^\+?\d+$/);

	if(!nameRegex.test(entry.firstName) || entry.firstName.length > 50 || entry.firstName.length === 0)
		return false;
	if(!nameRegex.test(entry.surname) || entry.surname.length > 50 || entry.surname.length === 0)
		return false;
	// if(!numberRegex.test(entry.contact_number) || entry.contact_number<7 || entry.contact_number>15)
	// 	return false;
	if(!emailRegex.test(entry.email))
		return false
	return true;
}