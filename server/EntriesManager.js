const fs = require("fs");
const path = require("path");

class EntriesManager{
	constructor()
	{
		this.filePath = path.resolve(__dirname, "entries.json");
		this.createFile();
	}
	createFile()
	{
		if(fs.existsSync(this.filePath)){
			console.log("Entries file already exists");
		}else{
			this.writeToFile()
				.then(()=>{
					console.log(`Entries file created: ${this.filePath}`)
				})
				.catch((err)=>{
					throw err;
				})
		}
	}
	readFromFile()
	{
		return new Promise((resolve, reject)=>{
			fs.readFile(this.filePath, "utf8", (err, data)=>{
				if(err){reject(err)};
				resolve(data);
			});
		})
	}
	writeToFile(data = [])
	{
		return new Promise((resolve, reject)=>{
			fs.writeFile(this.filePath, JSON.stringify(data), (err)=>{
				if(err){reject(err);}
				resolve(data);
			});
		})
	}
	async addEntry(entry)
	{
		let currentEntries = await this.readFromFile();
		currentEntries = JSON.parse(currentEntries);

		return new Promise((resolve, reject)=>{
			if(this.entryIsValid(entry, currentEntries)){
				currentEntries.push(entry);
				this.writeToFile(currentEntries);
				resolve(JSON.stringify(currentEntries));gg
			}
			reject("Entry name already exists");
		})
	}
	async deleteEntry(entry)
	{
		let currentEntries = await this.readFromFile();
		currentEntries = JSON.parse(currentEntries);

		return new Promise((resolve, reject)=>{
			let index = 0;
			for(let {firstName, surname} of currentEntries){
				if(entry.firstName.toUpperCase() === firstName.toUpperCase() && entry.surname.toUpperCase() === surname.toUpperCase()){
						currentEntries.splice(index, 1);
						this.writeToFile(currentEntries);	
						return resolve(JSON.stringify(currentEntries));
					}
				index++;
			}
			return reject("Something's wrong. Did you try to delete an entry that does not exist?");
		})
	}
	async editEntry(entry, newEntry)
	{
		let currentEntries = await this.readFromFile();
		currentEntries = JSON.parse(currentEntries);

		return new Promise((resolve, reject)=>{
			let index = 0;
			for(let {firstName, surname} of currentEntries){
				if(entry.firstName.toUpperCase() === firstName.toUpperCase() && entry.surname.toUpperCase() === surname.toUpperCase())
					{
						currentEntries[index] = newEntry;
						this.writeToFile(currentEntries);
						return resolve(JSON.stringify(currentEntries));
					}
				index++;
			}
			return reject("Something's wrong. Did you try to edit an entry that does not exist?");
		})
	}
	entryIsValid(entry, array)
	{
		for(let {firstName, surname} of array){
			if(entry.firstName.toUpperCase() === firstName.toUpperCase() && entry.surname.toUpperCase() === surname.toUpperCase())
				return false
		}
		return true;
	}
}

module.exports = new EntriesManager();