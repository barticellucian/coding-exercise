require("babel-register");
require("babel-polyfill");

const port = process.env.PORT || 3012;
const express = require("express");
const app = express();
const http = require('http').Server(app);
const path = require("path");
const io = require('socket.io')(http);
const EM = require("./EntriesManager.js");



app.use(express.static(path.resolve(__dirname,'../build')));
app.get("/", (req, res)=>{
	res.sendFile(path.resolve(__dirname,'../build/index.html'));
})


io.on("connection", (socket)=>{
	socket.on("get-entries", (data)=>{
		EM.readFromFile()
			.then((data)=>{
				io.emit("gotEntries", data);
			})
			.catch((err)=>{
				io.emit("serverError", `There was a problem reading from file: ${err}`);
			})
	});
	socket.on("add-entry", (data)=>{
		let entry = JSON.parse(data);
		EM.addEntry(entry)
			.then((resolved)=>{
				io.emit("gotEntries", resolved);
			})
			.catch((err)=>{
				io.emit("serverError", `There was a problem adding the entry: ${err}`);
			})
	});
	socket.on("delete-entry", (data)=>{
		let entry = JSON.parse(data);
		EM.deleteEntry(entry)
			.then((resolved)=>{
				io.emit("gotEntries", resolved);
			})
			.catch((err)=>{
				io.emit("serverError", `There was a problem deleting the entry: ${err}`);
			})
	});
	socket.on("edit-entry", (data)=>{
		let entries = JSON.parse(data);
		EM.editEntry(...entries)
			.then((resolved)=>{
				io.emit("gotEntries", resolved);
			})
			.catch((err)=>{
				io.emit("serverError", `There was a problem editing the entry: ${err}`);
			})
	})
})


module.exports = http;