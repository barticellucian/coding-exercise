const server = require("../server/server.js");
const assert = require("assert");
const http = require("http");
const io = require("socket.io-client");
const socketURL = "http://localhost:3012";
const EM = require("../server/EntriesManager.js");
var client = null;
const dummyContent = {
        	firstName: "Dummy", 
        	surname: "Tester",
        	contact_number: "123456789",
        	email: "dummy@tester.com"
        }

beforeEach(()=>{
	server.listen(3012);
	client = io.connect(socketURL);
});

afterEach(()=>{
	server.close();
	client.disconnect();
	client = null;
});

describe("get-entries over sockets", function(){
	it("should return an array of entries on connect", function(done){
	   client.emit('get-entries', "");
	   client.on("gotEntries", function(data){
	   		assert.equal(JSON.parse(data).constructor, Array);
	   		done();
	   })
	})
});

describe("add-entry over sockets", function(){
	it("should add an entry to the file", function(done){
		client.emit('add-entry', JSON.stringify(dummyContent));
		client.on("gotEntries", function(data){
			let dummyContentExists = JSON.parse(data).reduce((acc, cv, i, arr)=>{
				return (cv.firstName === dummyContent.firstName && cv.surname === dummyContent.surname); 
			}, false);

			assert.equal(dummyContentExists, true);
			done();
		});
	});
});

describe("add-entry(duplicate) over sockets", function(){
	it("should fail to add entries that are not unique", function(done){
		//here we try to add the same content again
		client.emit('add-entry', JSON.stringify(dummyContent));
		client.emit('get-entries', "");
		client.on("gotEntries", function(data){
			count = 0;
			for(let {firstName, surname} of JSON.parse(data)){
				count = (firstName === dummyContent.firstName && surname === dummyContent.surname) ? count+1 : count;
			}
			assert.equal(count, 1);
			done();
		});
	});
});


describe("edit-entry over sockets", function(){
	it("should edit an existing entry in the file", (done)=>{
		let editedDummy = Object.assign({}, dummyContent);
		editedDummy.email = "dummy@edited.com";

		client.emit('edit-entry', JSON.stringify([dummyContent, editedDummy]));

		client.on("gotEntries", function(data){
			let dummyIsEdited = JSON.parse(data).reduce((acc, cv, i, arr)=>{
				return (cv.firstName === editedDummy.firstName && cv.surname === editedDummy.surname && cv.email === editedDummy.email); 
			}, false);

			assert.equal(dummyIsEdited, true);
			done();
		})
	});
});


describe("delete-entry over sockets", function(){
	it("should delete the entry from the file", function(done){
		client.emit("delete-entry", JSON.stringify(dummyContent));
		client.on("gotEntries", function(data){
			let dummyContentExists = JSON.parse(data).reduce((acc, cv, i, arr)=>{
				return (cv.firstName === dummyContent.firstName && cv.surname === dummyContent.surname); 
			}, false);

			assert.equal(dummyContentExists, false);
			done();
		});
	})
})